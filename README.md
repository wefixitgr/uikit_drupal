# How to setup the theme #

## Dependancies

* Ruby, Rubygems, `gem install sass` (for compiling sass)
* Nodejs, npm, gulp (for compiling sass js etc) [How to](http://www.reallifedigital.com/blog/how-we-got-custom-drupal-8-theme-and-running-sass-singularity-breakpoint-livereload-and-gulp)
* Git (for obvious reasons)

## On a live/new Drupal Project

* Clone the repo as a submodule under themes/custom/
```
$ git submodule add https://ngal@bitbucket.org/wefixitgr/uikit_drupal.git themes/custom/uikit_drupal
$ git submodule init
```
* Copy the theme folder to another theme folder in order to be versioned by the main drupal repo (remove template dir after copy)
* Change the names on the new folder/theme and make it as a child theme of the main.
* Initialize npm, inside theme folder run `npm install gulp --saved-dev`
* Don't forget to change default gulpfile location...

## If you cloned the parent project

* In case you cloned the parent git project then information about submodule will be there but the files **wont!** Run the following to update the submodule hence download its code
```
$ git submodule update
```

## For adding new features to the theme

* Clone it to your computer
* Make changes
* Test it on a local d8 installation that is not versioned so you can play freely with the theme folder being the only versioned thing in the test project
* Initialize npm, inside theme folder run `npm install gulp --save-dev`
* Push
* Profit.

### How to set up auto reload

Requirements:
* Ruby >= 2.3
* Guard (https://github.com/guard/guard) `gem install guard`
* Guard-livereload (https://github.com/guard/guard-livereload) `gem install guard-livereload`
* Chrome extension (Google it)
* updated gulpfile (documentation updates on that soon...)

### What is this repository for? ###

* This is a base theme of uikit created by wefixit
* Current Version: 1.0
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)


### Contribution guidelines ###

* Only accepting contributions by pull requests
* Always use a feature branch
* Some admin will review your code
* If all good will be pushed to main repo

### Who do I talk to? ###

* Nick Galatis