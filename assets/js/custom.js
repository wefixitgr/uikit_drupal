/**
 * Use this custom javascript file to add customizations.
 * A placeholder console log is added by default.
 * Please document your code clearly!
 */

(function ($) { // jQuery no conflict init
    'use strict'; // Drupal JS Strict Standards with closure
        // Simple console log to see if it works and skip errors
        $(window).on('load', function () {
          console.log('Ready for takeoff. wefixit.')
        });

        $(document).ready(function () {
            // Find and change offcanvas classes to render
            // the mobile menu for uikit. This means we
            // dont need another twig file-block for mobile
            // menu, simplifying things.
            $('#offcanvas')
                .find('.uk-navbar-nav')
                .removeClass('uk-navbar-nav')
                .addClass('uk-nav uk-nav-offcanvas');
        });

    // close any system message on click
    $('div[role="contentinfo"]').on('mousedown', function () {
      console.log('clicked bitch');
      $(this).fadeOut();
    });

})(jQuery);
