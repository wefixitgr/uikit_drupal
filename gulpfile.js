/**
 * Main Gulp File for theme tasks
 * such as scss conversion to css
 * and js uglification etc
 */

require('es6-promise').polyfill(); // add this if you have errors

var gulp = require('gulp');

// Load the packages
var sass = require('gulp-ruby-sass');
var autoprefixer = require('gulp-autoprefixer');
var minifycss = require('gulp-minify-css');
var uglify = require('gulp-uglifyjs');
var livereload = require('gulp-livereload');

// Default task for testing
gulp.task('default', function () {
  // place code here...
  // left for testing...
});

// Sass Conversion to css task
gulp.task('sass', function () {
  // The main scss file location
  return sass('assets/sass/wefixit.scss', {
    style:'expanded',
    "sourcemap=none":true,
    "line-comments":true
  })
  // Autoprefixer task
      .pipe(autoprefixer({
        browsers:['last 2 versions']
      }))
      // Final File destination
      .pipe(gulp.dest('assets/css'));
});

// Javascript uglify task
gulp.task('scripts', function () {
  return gulp.src('assets/js/custom.js')
      .pipe(uglify())
      .pipe(gulp.dest('assets/js/'));
});

// Watch task that watches the tasks above and automates the procedure
gulp.task('watch', function () {
  livereload.listen();
  gulp.watch('assets/sass/*.scss', ['sass']);
  gulp.watch('assets/js/*.js', ['scripts']);
  gulp.watch(['./assets/css/wefixit.css', './**/*.twig', './assets/js/*.js'], function (files){
    livereload.changed(files)
  });
});